RULES = {
  "A" => {
    unitPrice: 50,
    specialPrice: {
      amount: 3,
      price: 130
    }
  },
  "B" => {
    unitPrice: 30,
    specialPrice: {
      amount: 2,
      price: 45
    }
  },
  "C" => {
    unitPrice: 20
  },
  "D" => {
    unitPrice: 15
  }
}

class CheckOut
  def initialize(rules)
    @rules = rules
    @items = Hash.new(0)
  end

  def total
    @items.inject(0) do |total, (item)|
      total += calculate_discount(@rules[item], item)
    end
  end

  def scan(item)
    @items[item] += 1
  end

  def calculate_discount(rule, item)
    if rule[:specialPrice] && rule[:specialPrice][:amount] === @items[item]
      return rule[:specialPrice][:price]
    elsif rule[:specialPrice] && rule[:specialPrice][:amount] < @items[item]
      modular = @items[item] % rule[:specialPrice][:amount]
      discounted = (@items[item] / rule[:specialPrice][:amount]).floor * rule[:specialPrice][:price]
      return discounted + (modular * rule[:unitPrice])
    end

    @rules[item][:unitPrice] * @items[item]
  end
end

# Item   Unit      Special
#         Price     Price
#  --------------------------
#    A     50       3 for 130
#    B     30       2 for 45
#    C     20
#    D     15
